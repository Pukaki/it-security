import datetime
import os
import argparse
import sys

class Exercise00:
	STUDENT_NAME = "Reiko Lettmoden"

	# Test failed aus unerklaerlichen Gruenden
	# Function Object
	def __call__(self, **kwargs):
		sortedElements = sorted(kwargs.items(), key=lambda tup: tup[0]) # sort by first key
		for name, value in sortedElements:
			if value is None:
				value = "None"
			print("{} = {}\n".format(name, value), end = "")

	@property
	def txt(self):
		return self._txt
	
	# constructor
	def __init__(self, extraInfo = ""):
		if(len(extraInfo) > 17):
			self._txt = extraInfo[0:17] + "..." 
		else:
			self._txt = extraInfo + "..."

	def deadline(dateFormat):
		deadlineDate = datetime.datetime(2019, 11, 13, 11, 59, 0, 0)
		return deadlineDate.strftime(dateFormat)

	def format(self, mode):
		if mode in "order":
			return "{2} - {1} - {0}"
		elif mode in "dict":
			return "x, y = ({x:.1f}, {y:.4f})"

	# test failed weil __init__.py fehlt
	# lists all objects
	def listfiles(self, path, type=None):
		files = [f for f in os.listdir(path)]
		
		for file in files:
			if os.path.isdir(path+"\\"+file):
				yield from self.listfiles(path+"\\"+file, type)
			else:
				if type is not None:
					if file.endswith(type):
						yield file
				else:
					yield file


def main():
	print(Exercise00.deadline("%H:%M %d.%m.%Y"))
	ex = Exercise00("abcdefghijklmnopqrstuvwxyz")
	print(len(ex.txt))
	print(ex.format("dict"))
	print(ex.format("dict").format(x=41.123, y=71.091))
	print(ex.format("order").format("third", "second", "first"))

	fileWalker = ex.listfiles("C:\\Users\\Reiko\\Documents\\Diverse Dokumente\\Studium\\WS1920\\IT Security\\Aufgabe 0", ".py")
	for file in fileWalker:
		print(file)

	ex(c=None, a=1, d=4, b="2")

	parser = argparse.ArgumentParser(add_help=True)
	parser.add_argument("-b", "--bool", help="An optional boolean ﬂag wit default: False", default=False, action="store_true")
	parser.add_argument("-f", "--float", type=float, help="An optional parameter of type float with default: 0", default=0)
	parser.add_argument("-i", "--int", type=int, help="An optional parameter of type int with default: 0.0", default=0.0)

	args = parser.parse_args()
	if args.bool:
		#if(args.bool.lower() in "true" or args.bool.lower() in "false"):
		#	boolString = "True" if args.bool.lower() in "true" else "False" 
		#	print("Bool: {}".format(boolString))
		print("Bool: {}".format(args.bool))
	if args.float:
		print("Float: {}".format(args.float))
	if args.int:
		print("Int: {}".format(args.int))

	args = parser.parse_args()
	sys.exit(42)

if __name__ == "__main__":
	main()s