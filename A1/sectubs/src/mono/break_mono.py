

# wikipedia
relativeLetterDistribution = {'a': '8.167', 'b': '1.492', 'c': '2.202', 'd': '4.253', 'e': '12.702', 'f': '2.228', 'g': '2.015', 'h': '6.094', 'i': '6.966', 'j': '0.153', 'k': '1.292', 'l': '4.025', 'm': '2.406', 'n': '6.749', 'o': '7.507', 'p': '1.929', 'q': '0.095', 'r': '5.987', 's': '6.327', 't': '9.356', 'u': '2.758', 'v': '0.978', 'w': '2.560', 'x': '0.150', 'y': '1.994', 'z': '0.077'}
relativeStartLetterDistribution = {'a': '11.682', 'b': '4.434', 'c': '5.238', 'd': '3.174', 'e': '2.799', 'f': '4.027', 'g': '1.642', 'h': '4.200', 'i': '7.294', 'j': '0.511', 'k': '0.856', 'l': '2.415', 'm': '3.826', 'n': '2.284', 'o': '7.631', 'p': '4.319', 'q': '0.222', 'r': '2.826', 's': '6.686', 't': '15.978', 'u': '1.183', 'v': '0.824', 'w': '5.497', 'x': '0.045', 'y': '0.763', 'z': '0.045'}
mostUsedWords = ['the', 'be', 'to', 'of', 'and', 'a', 'in', 'that', 'have', 'I', 'it', 'for', 'not', 'on', 'with', 'he', 'as', 'you', 'do', 'at', 'this', 'but', 'his', 'by', 'from', 'they', 'we', 'say', 'her', 'she', 'or', 'an', 'will', 'my', 'one', 'all', 'would', 'there', 'their', 'what', 'so', 'up', 'out', 'if', 'about', 'who', 'get', 'which', 'go', 'me', 'when', 'make', 'can', 'like', 'time', 'no', 'just', 'him', 'know', 'take', 'people', 'into', 'year', 'your', 'good', 'some', 'could', 'them', 'see', 'other', 'than', 'then', 'now', 'look', 'only', 'come', 'its', 'over', 'think', 'also', 'back', 'after', 'use', 'two', 'how', 'our', 'work', 'first', 'well', 'way', 'even', 'new', 'want', 'because', 'any', 'these', 'give', 'day', 'most', 'us']

import argparse
import cipherInfo
import sys
import ioHelper

debug = False
KEY = list("abcdefghijklmnopqrstuvwxyz")
OFFSET = 97


def parseArgs():
	parser = argparse.ArgumentParser(add_help=True)
	parser.add_argument("FILE", type=str, help="An inputFile where the result of the cipher operation is stored")

	encryptionKey = ""
	decryptionKey = ""
	outFile = ""
	text = ""

	args = parser.parse_args()
	if args.FILE:
		text = ioHelper.readFile(args.FILE)
		if text == "":
			print("Invalid Input file")
			return None
		
	cipherInformation = cipherInfo.cipherInfo(text, encryptKey = encryptionKey, decryptKey = decryptionKey, outFile = outFile)
	if debug:
		print("encryptionKey: {}".format(cipherInformation.encryptKey))
		print("decryptionKey: {}".format(cipherInformation.decryptKey))
		print("outFile: {}".format(cipherInformation.outFile))
		print("text: {}".format(cipherInformation.text))
	return cipherInformation

def encrypt(text, key):
	encryptedText = ""
	offset = 97
	for character in text:
		encrCharPosition = ord(character) - offset
		encrCharValue = key[encrCharPosition]
		encryptedText += encrCharValue
	return encryptedText

def decrypt(text, key):
	decryptedText = ""
	offset = 97
	for character in text:
		keyIndex = key.find(character)
		if keyIndex == -1:
			print("Unexpected error: key or input wrong")
			print(character)
			print(key)
			exit()

		decryptedText += chr(offset + keyIndex)
	return decryptedText

def calcTextDistribution(text):
	#stackoverflow
	def truncate(f, n):
	    '''Truncates/pads a float f to n decimal places without rounding'''
	    s = '{}'.format(f)
	    if 'e' in s or 'E' in s:
	        return '{0:.{1}f}'.format(f, n)
	    i, p, d = s.partition('.')
	    return '.'.join([i, (d+'0'*n)[:n]])
	#stackoverflow end
	dist = {}
	textLength = len(text)
	for letter in "abcdefghijklmnopqrstuvwxyz":
		dist[letter] = truncate((text.count(letter)/textLength) * 100, 3)
	print(dist)
	return dist

def findMostRepetitions(text, key = None):
	out = []
	for i in range(2, 10):
		repitions = findWordRepition(text, i)
		if key is not None:
			for repition in repitions:
				if repition[0].startswith(key):
					out.append(repition)
		else:
			if debug:
				print("_____________________")
				print(i)
				print(repitions)

	return out

def findWordRepition(text, k):
	wordDict = {}
	for i in range (0, len(text) - k):
		word = text[i:i+k]
		wordDict[word] = text.count(word)
	sortedTuples = sorted(wordDict.items(), key=lambda kv: kv[1])
	sortedTuples.reverse()

	limiter = 10 if len(sortedTuples) > 10 else len(sortedTuples)
	return sortedTuples[0:limiter]

def break_mono(cipherInformation):
	dis = calcTextDistribution(cipherInformation.text)
	# findMostRepetitions(cipherInformation.text) for debug porpuse

	the = findWordRepition(cipherInformation.text, 3)[0]
	KEY[ord('t') - OFFSET] = the[0][0]
	KEY[ord('h') - OFFSET] = the[0][1]
	KEY[ord('e') - OFFSET] = the[0][2]

	thEncrypted = the[0][0] + the[0][1]
	theEncrypted = the[0][0] + the[0][1] + the[0][2]

	startsWithTh = findMostRepetitions(cipherInformation.text, thEncrypted)
	print(startsWithTh)

	# get other vocals: ??? difficult
	startsWithoutThe = []
	for th in startsWithTh:
		if not th[0].startswith(theEncrypted):
			startsWithoutThe.append(th)
	print(startsWithoutThe)
	# a

	#find out other dist with th start
	# 'a' 't' most commen word starts
	# 'e' most common letter
	# 'the' most common word

	# idea 1:
	# deduct word starts -> 'a' 'o' 's'
	# deduct other common letters "h" 'j' 's' 'r' 'n'
	# deduct other words -> deduct other letters
	
	# idea 2:
	# make word crawlers

	pass


if __name__ == "__main__":
	cipherInformation = parseArgs()
	if cipherInformation == None:
		print("Could not initialize mono")
		exit()
	else:
		break_mono(cipherInformation)