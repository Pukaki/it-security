import os.path
import re

def readFile(path):
	# if not os.path.isfile(path):
	# 	print("File does not exist")
	# 	return None
	with open(path, "r") as f:
		lines = [line.rstrip("\n") for line in f]
		outString = ""
		for line in lines:
			for character in line:
				if character.lower().isalpha():
					outString += character.lower()
		return outString
		
def writeFile(path, text):
	with open(path, "w") as f:
		f.write(text)

if __name__ == '__main__':
	readFile("ex01_mono.plaintext")