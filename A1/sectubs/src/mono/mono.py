import argparse
import cipherInfo
import sys
import ioHelper

debug = False

def parseArgs():
	parser = argparse.ArgumentParser(add_help=True)
	parser.add_argument("--encrypt", type=str, help="Used to apply a mono encryption with the given key")
	parser.add_argument("--decrypt", type=str, help="Used to apply a mono decryption with the given key")
	parser.add_argument("--out", type=str, help="An optional outputFile where the result of the cipher operation is stored")
	parser.add_argument("FILE", type=str, help="An inputFile where the result of the cipher operation is stored")

	encryptionKey = ""
	decryptionKey = ""
	outFile = ""
	text = ""

	args = parser.parse_args()
	if args.encrypt:
		encryptionKey = args.encrypt
	if args.decrypt:
		decryptionKey = args.decrypt
	if args.encrypt and args.decrypt:
		print("Either encrypt or decrypt, not both")
		return None
	if not args.encrypt and not args.decrypt:
		print("Either encrypt or decrypt, not nothing of both")
		return None
	if args.out:
		outFile = args.out
	if args.FILE:
		text = ioHelper.readFile(args.FILE)
		if text == "":
			print("Invalid Input file")
			return None
		

	cipherInformation = cipherInfo.cipherInfo(text, encryptKey = encryptionKey, decryptKey = decryptionKey, outFile = outFile)
	if debug:
		print("encryptionKey: {}".format(cipherInformation.encryptKey))
		print("decryptionKey: {}".format(cipherInformation.decryptKey))
		print("outFile: {}".format(cipherInformation.outFile))
		print("text: {}".format(cipherInformation.text))
	return cipherInformation

def encrypt(text, key):
	encryptedText = ""
	offset = 97
	for character in text:
		encrCharPosition = ord(character) - offset
		encrCharValue = key[encrCharPosition]
		encryptedText += encrCharValue
	return encryptedText

def decrypt(text, key):
	decryptedText = ""
	offset = 97
	for character in text:
		keyIndex = key.find(character)
		if keyIndex == -1:
			print("Unexpected error: key or input wrong")
			print(character)
			print(key)
			exit()

		decryptedText += chr(offset + keyIndex)
	return decryptedText

def mono(cipherInformation):
	res = ""
	if cipherInformation.encryptKey == "":
		res = decrypt(cipherInformation.text, cipherInformation.decryptKey)
	else:
		res = encrypt(cipherInformation.text, cipherInformation.encryptKey)

	if debug:
		print(res)
	if cipherInformation.outFile != "":
		ioHelper.writeFile(cipherInformation.outFile, res)
	else:
		print(res)

if __name__ == "__main__":
	cipherInformation = parseArgs()
	if cipherInformation == None:
		print("Could not initialize mono")
		exit()
	else:
		mono(cipherInformation)
