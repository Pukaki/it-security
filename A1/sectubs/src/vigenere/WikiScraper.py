# :)
# https://automatetheboringstuff.com/chapter11/

import requests
import bs4

r = requests.get("https://en.wikipedia.org/wiki/Letter_frequency")
#print(r.text)
soup = bs4.BeautifulSoup(r.text, "lxml")
tables = soup.select("div > table")
matches = []
for table in tables:
	sub = table.select('th[colspan="2"]')
	if len(sub) != 0:
		matches += table

# first table relative letter frequency
colsLetter = tables[0].select('td[align="center"]')
colsPerc = tables[0].select('td[align="right"]')

relativeData = {}

for i in range(0, len(colsLetter)):
	letter = colsLetter[i].select("b")[0].getText()
	percent = colsPerc[i].getText().rstrip("\n%")
	relativeData[letter] = percent

# second table relative start letter frequency
colsLetter = tables[1].select('td[align="center"]')
colsPerc = tables[1].select('td[align="right"]')

relativeData = {}

for i in range(0, len(colsLetter)):
	letter = colsLetter[i].select("b")[0].getText()
	percent = colsPerc[i].getText().rstrip("\n%")
	relativeData[letter] = percent

# print(relativeData)

# most used words
r = requests.get("https://en.wikipedia.org/wiki/Most_common_words_in_English")
soup = bs4.BeautifulSoup(r.text, "lxml")
table = soup.select("div > table")[0]

words = [i.getText() for i in table.select(".extiw")]
print(words)


# first table relative letter frequency
# colsLetter = tables[0].select('td[align="center"]')
# colsPerc = tables[0].select('td[align="right"]')