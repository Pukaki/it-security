class cipherInfo:
	@property
	def text(self):
		return self._text

	@property
	def outFile(self):
		return self._outFile

	@property
	def encryptKey(self):
		return self._encryptKey

	@property
	def decryptKey(self):
		return self._decryptKey
	
	# constructor
	def __init__(self, text, outFile = "", encryptKey = "", decryptKey = ""):
		self._text     = text
		self._outFile    = outFile
		self._encryptKey = encryptKey
		self._decryptKey = decryptKey
	
