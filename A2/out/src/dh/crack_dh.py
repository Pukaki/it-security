def crackit(generator, modulus, alice, bob):
    for i in range(0, modulus):
        if((generator**i)%modulus == alice):
            return bob**i%modulus

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', type=int, help='generator')
    parser.add_argument('-n', type=int, help='Modulus')
    parser.add_argument('--alice', type=int, help='publically transferred interger from Alice')
    parser.add_argument('--bob', type=int, help='publically transferred interger from Bob')
	
    args = parser.parse_args()
    key = crackit(args.g, args.n, args.alice, args.bob)
    print(key)

main()
