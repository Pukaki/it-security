def prim_liste(mod):
    liste = []
    for i in range(2, mod):
        is_prim = 1
        for j in liste:
            if(i%j == 0):
                is_prim = 0 
        if(is_prim==1):
            liste.append(i)
    return liste

def crackmod(mod, liste):
    pq = []
    for i in liste:
        if(mod%i == 0):
            pq.append(i)
            pq.append(mod/i)
            return pq          

def get_euler(pq):
    p = pq[0]-1
    q = pq[1]-1
    return p*q

def crackit(public_key, mod):
    liste = prim_liste(mod)
    pq = crackmod(mod, liste)
    euler = get_euler(pq)
    for i in range(1, mod):
        if(((public_key*i)%euler)==1):
            return i

def main():
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('-e', type=int, help='Public key')
	parser.add_argument('-n', type=int, help='Modulus')
	parser.add_argument('--ciphertext', type=int, help='Ciphertext')
	
	args = parser.parse_args()
	decrypt_key = crackit(args.e, args.n)
	plaintext = (args.ciphertext**decrypt_key)%args.n
	print(plaintext)
	
main()
    
    
    
