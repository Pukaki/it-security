#!/usr/bin/env python3

'''
@author: Christian Wressnegger
'''

import os
import unittest

unittest.TestLoader.sortTestMethodsUsing = None
PYTHON = "python3"
PYERROR = "For running your solutions we call '{}'.\nThe name might be different for your installation (e.g. on Windows)\n"


class Ex07(unittest.TestCase):

    TASKS = 0

    def vuln_read_urls(self, c):
        my_dir = os.path.dirname(os.path.abspath(__file__))
        try:
            cmd = None
            with open(os.path.join(my_dir, "{}.sh".format(c)), 'r') as f:
                for line in f:
                    cmd = line

                    self.assertTrue(
                        cmd and cmd.startswith("./"), "Please start the command line with the local directory './")

            self.assertTrue(cmd, "No command line string included :/")

        except IOError:
            self.assertTrue(False, "Unable to read file")

    def test_01_stack_c(self):
        self.vuln_read_urls("stack-c")
        Ex07.TASKS += 1

    def test_02_local_a(self):
        self.vuln_read_urls("local-a")
        Ex07.TASKS += 1

    def test_02_local_b(self):
        self.vuln_read_urls("local-b")
        Ex07.TASKS += 1

    def test_02_local_c(self):
        self.vuln_read_urls("local-c")
        Ex07.TASKS += 1

    def test_XX(self):
        if Ex07.TASKS > 0:
            print(
                "[*] Your submission contains solutions for {} out of {} tasks! 👍".format(Ex07.TASKS, 4))
        else:
            print("[*] Unfortunately, there are not valid solutions (yet 😉)")


if __name__ == "__main__":
    unittest.main()
