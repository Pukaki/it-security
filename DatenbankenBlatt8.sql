Notiz: alle Attribute von SELECT muessen auch im GROUP BY auftreten
Unklar: Klammerung bei Mengenoperationen

8.1

b)

SELECT m.name
FROM modul m
    JOIN abschliessen a ON m.id = a.m
GROUP BY m.id, m.name
HAVING ABG(a.note) < 2.7

c)

SELECT m.id, m.name
FROM modul m
    JOIN voraussetzungen v ON m.id = v.id
WHERE u.m = v.braucht

d)
SELECT COUNT(DISTINCT p.vorname)
FROM person p

e)
SEKECT p.vorname, p.nachname
FROM person p
    JOIN kursk ON p.id = k.p
    LEFT JOIN student s ON p.id = s.pid
WHERE s.pid is NULL

8.2

a)
SELECT v1.m, v2.m
FROM voraussetzung v1
    JOIN voraussetzung v12 ON v12.m = v1.braucht
    JOIN voraussetzung v22 ON v22.braucht = v12.braucht
    JOIN voraussetzung v2   ON v2.braucht = v22.m
EXCEPT 
    SELECT v1.m, v2.m
    FROM  voraussetzung v1 
        JOIN voraussetzung v2 ON v1.braucht = v2.braucht

b)
SELECT p.vorname, p.nachname
FROM person p
    JOIN professor p prof ON p.id = prof.pid
    JOIN kurs k          ON k.p = p..id
WHERE prof.fachrichtung = ‘information systems’ AND m.name = ‘RDB1’


c)
SELECT p.vorname, p.nachname
FROM person p
    JOIN kurs k1 ON p.id = k1.p
    JOIN kurs k2 ON k1.m = k2.m
WHERE k1.p <> k2.p

8.3

a)
SELECT p.vorname, p.nachname
FROM person p
    JOIN student s      ON p.id = s.pid
    JOIN abschliessen a ON a.s = p.id
    JOIN modul m        ON a.m = m.id
WHERE s.semesterzahl = 3 AND a.note <= 4.0
GROUP BY p.id, p.vorname, p.nachname
HAVING sum(m.credits) < 30
UNION
    SELECT p.vorname, p.nachname
    FROM person p
        JOIN student s p.id = s.pid
    WHERE s.semesterzahl = 3 AND NOT EXISTS
        (SELECT *
        FROM abschliessen a
        WHERE a.s = p.id AND a.note <= 4.0);

Note: semikolon bei mehreren Abfragen hintereinander

b)
SELECT AVG (a.note)
FROM abschliessen a
    JOIN kurs k         ON a.j = k.jar AND a.m = k.m
    JOIN person p       ON  k.p = p.id
WHERE a.note <= 4.0 AND p.vorname = ‘David’ AND p.nachname = ‘Bower’
GROUP BY p.id

c)
SELECT k1.jahr, k1.m
FROM kurs k1
    JOIN kurs k2            ON k1.m = k2.m
    JOIN professor prof2    ON k2.p = prof2.pid
WHERE k2.jahr = k1.jahr-1 AND prof1.fachrichtung <> prof2.fachrichtung

d)
WITH kpaj AS (
        SELECT k.m, j.p, COUNT(k.jahr) AS count
        FROM kurs k
        GROUP BY k.m, k.p),
    kpmax AS (
        SELECT kpaj.m, MAX(kpaj.count) AS max
        FROM kpaj
        GROUP BY kpaj.m)
SELECT km.m p.vorname, p.nachname, prof.fachrichtung
FROM person p
    JOIN professor prof ON p.id = prof.pid
    JOIN kpaj kj        ON p.id = kj.p
    JOIN kpmax km       ON kj.m = km.m
WHERE kj.count = km.max

e)

WITH studcred AS( 
        -- alle studenten die etwas bestanden haben 
        SELECT a.j, a.s, SUM(m.credits) as sum
        FROM abschliessen a
            JOIN modul m ON a.m = m.id
        WHERE a.note <= 4.0
        GROUP BY a.j, a.s),
    avgcred AS(
        SELECT sc.j,
        AVG(sc.sum) as avg
        FROM studcred sc
        GROUP BY sc.j
        )
SELECT sc.j, p.vorname, p.nachname
FROM person p
    JOIN studcred sc ON p.id = sc.j
    JOIN avgcred ac  ON sc.j = ac.j
WHERE sc.sum > ac.avq

f)

WITH cc AS(
        SELECT s.pid, COUNT(a.m) AS count
        FROM student s
            LEFT JOIN abschliessen a ON s.pid = a.s
        WHERE a.note <= 4.0 OR a.m IS NULL
        GROUP BY s.pid),

g)

SELECT p.vorname, p.nachname
FROM person p
	JOIN students s ON p.id=s.id
WHERE NOT EXISTS( SELECT *
				  FROM Abschliessen a
					  JOIN Modul m ON a.m=m.id
				  WHERE a.s=p.id
					  AND m.name='RDB1'
					  AND a.note<=4.0
				)
	AND NOT EXISTS( SELECT *
				 	FROM Modul m
						JOIN Abschliessen a ON a.m=m.id
				  	WHERE m.name='RDB1'
						AND NOT EXISTS( SELECT *
										FROM Abschliessen a
										WHERE a.s=p.id
											AND a.m=v.braucht
											AND a.note <= 4.0
									  )
				  )
    
Note: 0 haendisch eintragen -> dann avg bestimmen

Note: in der Klausur fuer Statements Kleinschreibung erlaubt -> mehr Zeit
